terraform {
  backend "s3" {
    bucket         = "terraform-pl-state"
    key            = "valstore/terraform.tfstate"
    region         = "eu-west-1"
  }
}